﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace CVARP
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            imgWorker = new ImageWorker();
            imgWorker.errorEvent += ImgWorker_errorEvent;
            imgWorker.successEvent += ImgWorker_successEvent;
            imgWorker.msgEvent += ImgWorker_msgEvent;
            Application.Current.Exit += Current_Exit;
            ListViewLog.ItemsSource = imgWorker.Logs;
        }

        private void ImgWorker_msgEvent(object sender, MessageEventArgs e)
        {
            //ListViewLog.Items.Refresh();
            ListViewLog.SelectedIndex = ListViewLog.Items.Count - 1;
        }

        private void Current_Exit(object sender, ExitEventArgs e)
        {
            foreach (var item in imgWorker.images)
            {
                if(item.Path!=imgWorker.origImages[item.Index].Path)
                    File.Delete(item.Path);
            }
        }

        private void ImgWorker_successEvent(object sender, SuccessOperationEventArgs e)
        {
            
            ListViewImages.ItemsSource = imgWorker.images;
            ListViewImages.SelectedIndex = 0;

            if (Hint.Visibility == Visibility.Visible)
            {
                Hint.Visibility = Visibility.Hidden;
                // FirstScroll.Visibility = Visibility.Visible;
                Image1.Visibility = Visibility.Visible;
                ListViewImages.Visibility = Visibility.Visible;
                ListViewLog.Visibility = Visibility.Visible;
                //StackBtns.Visibility = Visibility.Visible;
            }
       
        }

        private void ImgWorker_errorEvent(object sender, ErrorEventArgs e)
        {
            MessageBox.Show(e.Message,e.Caption, e.mbButtons, e.mbImage);
            
        }

        ImageWorker imgWorker;
       

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.CheckPathExists = true;
            dlg.CheckFileExists = true;
            dlg.Multiselect = true;
            dlg.DefaultExt = ".png";
            dlg.Filter = "Images (*.png,*.jpg,*.bmp)|*.png; *.jpg";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                if (dlg.FileNames.Length > 1)
                {
                    imgWorker.LoadImages(dlg.FileNames.ToList<string>());
                }
            }
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            CalibrationWindow cw = new CalibrationWindow(imgWorker);
            cw.ShowDialog();
        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            imgWorker.Reset();
        }

        private void MenuItem_Click_4(object sender, RoutedEventArgs e)
        {
            imgWorker.CreateDepth();
        }

        private void MenuItem_Click_5(object sender, RoutedEventArgs e)
        {
            WindowAbout ab = new WindowAbout();
            ab.ShowDialog();
        }

        private void ListViewImages_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Image1.Source = imgWorker.images[((IndexedImage)e.AddedItems[0]).Index].Image;
            }
            catch
            {

            }
        }

        private void BtnAddImg_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.CheckPathExists = true;
            dlg.CheckFileExists = true;
            dlg.Multiselect = true;
            dlg.DefaultExt = ".png";
            dlg.Filter = "Images (*.png,*.jpg,*.bmp)|*.png; *.jpg";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                if (dlg.FileNames.Length >= 1)
                {
                    imgWorker.LoadImages(dlg.FileNames.ToList<string>());
                }
            }
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            imgWorker.ClearImages();
        }

        private void MenuItem_Click_6(object sender, RoutedEventArgs e)
        {
            imgWorker.FindKeypoints();
            ListViewImages.Items.Refresh();
        }

        private void MenuItem_Click_7(object sender, RoutedEventArgs e)
        {
            imgWorker.RectifyWithStereo();
        }

        private void MenuItem_Click_8(object sender, RoutedEventArgs e)
        {
            IFormatter formatter = new BinaryFormatter();
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            
            
            dlg.DefaultExt = ".3ds";
            dlg.Filter = "3D Reconstruction data file|*.3ds";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                Stream stream = new FileStream(dlg.FileName, FileMode.Create, FileAccess.Write, FileShare.None);
                formatter.Serialize(stream, imgWorker);
                stream.Close();
            }
           
        }

        private void MenuItem_Click_9(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.CheckPathExists = true;
            dlg.CheckFileExists = true;
            dlg.Multiselect = false;
            dlg.DefaultExt = ".3ds";
            dlg.Filter = "3D Reconstruction data file|*.3ds";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                if (dlg.FileNames.Length >= 1)
                {
                    IFormatter formatter = new BinaryFormatter();
                    Stream stream = new FileStream(dlg.FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                    imgWorker = (ImageWorker)formatter.Deserialize(stream);
                    stream.Close();

                }
            }
        }

        private double _val;

        
        private void ListViewLog_SizeChanged(object sender, SizeChangedEventArgs e)
        {
           if( e.WidthChanged)
            {
                var fe = (FrameworkElement)sender;
                var le = (ListView)fe;
                foreach (var item in le.Items)
                {
                    var itm = item as Log;
                    
                }
            }
        }

        private void MenuItem_Click_10(object sender, RoutedEventArgs e)
        {
            imgWorker.ClearImages();
        }
    }
}
