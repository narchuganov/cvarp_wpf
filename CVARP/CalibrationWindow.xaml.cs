﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OpenCvSharp.CPlusPlus;
namespace CVARP
{
    /// <summary>
    /// Логика взаимодействия для CalibrationWindow.xaml
    /// </summary>
    public partial class CalibrationWindow : System.Windows.Window
    {
        public CalibrationWindow(object imag)
        {
            InitializeComponent();
            imgWorker = (ImageWorker)imag;
            imgWorker.calibrateSetEvent += ImgWorker_calibrateSetEvent;
            imgWorker.errorEvent += ImgWorker_errorEvent;
        }

        private void ImgWorker_errorEvent(object sender, ErrorEventArgs e)
        {
            MessageBox.Show(e.Message, e.Caption,e.mbButtons,e.mbImage);
        }

        private void ImgWorker_calibrateSetEvent(object sender, CalibrateImageSetEventArgs e)
        {
            curIndex = 0;
            if (e.Success)
            {
                
                ImageCalib.Source = imgWorker.imageCal[curIndex];
                TextStatus.Text = "Откалибровано.";
            }
            else
                TextStatus.Text = "Неудалось откалибровать. Возможно, неверные параметры?";

        }
        byte curIndex=0;
        string[] fileNames;

        ImageWorker imgWorker;
        private void BtnOpenImag_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.CheckPathExists = true;
            dlg.CheckFileExists = true;
            dlg.Multiselect = true;
            dlg.DefaultExt = ".png";
            dlg.Filter = "Images (*.png,*.jpg,*.bmp)|*.png; *.jpg";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {

                //imgWorker.SetImageForCalibrate(dlg.FileName);
                fileNames = dlg.FileNames;
                imgWorker.SetForCalibrate(fileNames);
                ImageCalib.Source = imgWorker.imageCal[curIndex];
                TextStatus.Text = "Готов к калибровке.";
            }
        }

        private void BtnCalibrateCam_Click(object sender, RoutedEventArgs e)
        {
            TextStatus.Text = "Калибровка. Пожалуйста подождите...";
            imgWorker.SetImageForCalibrateStereo(fileNames,Convert.ToByte(TextBlockHeight.Text),Convert.ToByte(TextBlockWidth.Text));
            //imgWorker.SetImageForCalibrate(fileNames, Convert.ToByte(TextBlockHeight.Text), Convert.ToByte(TextBlockWidth.Text));
        }

        private void BtnLeft_Click(object sender, RoutedEventArgs e)
        {
            if (curIndex>0)
            {
                curIndex--;
                ImageCalib.Source = imgWorker.imageCal[curIndex];
            }
        }

        private void BtnRight_Click(object sender, RoutedEventArgs e)
        {
            if (curIndex < imgWorker.imageCal.Count-1)
            {
                curIndex++;
                ImageCalib.Source = imgWorker.imageCal[curIndex];
            }
        }

        private void TextBlockWidth_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
                e.Handled = true;

        }

        private void TextBlockHeight_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
                e.Handled = true;

        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
