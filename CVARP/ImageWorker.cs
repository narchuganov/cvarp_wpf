﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Windows;
//using OpenCvSharp.CPlusPlus;
using System.Windows.Media;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;
using System.Runtime.InteropServices;
using Emgu.CV.XFeatures2D;
using Emgu.CV.Features2D;
using Emgu.CV.Flann;
using System.IO;

namespace CVARP
{
    [Serializable]
    public class IndexedImage
    {
        public WriteableBitmap Image { get; set; }
        public WriteableBitmap KeypointImage { get; set; }
        public WriteableBitmap MatchedImage { get; set; }
        public int Index { get; set; }
        public string Path { get; set; }
        public VectorOfKeyPoint Keypoints { get; set; }
        public Mat Descriptors { get; set; }
        public Rectangle ROI { get; set; }
        public IndexedImage(WriteableBitmap img, int index,string path)
        {
            this.Image = img;
            this.Index = index;
            this.Path = path;



        }
    }

    public class Log
    {
        public string Message { get; set; }
        public string Date { get; set; }
        public Log(string msg, string date)
        {
            this.Date = date;
            this.Message = msg;
        }
    }

    public class ErrorEventArgs : EventArgs
    {
        public ErrorEventArgs(string message, string caption, MessageBoxImage mbImage, MessageBoxButton mbButtons)
        {
            this.Message = message;
            Caption = caption;
            this.mbImage = mbImage;
            this.mbButtons = mbButtons;
        }
        
        public string Message { get; set; }
        public string Caption { get; set; }
        public MessageBoxImage mbImage { get; set; }
        public MessageBoxButton mbButtons { get; set; }
    }

    public class SuccessOperationEventArgs:EventArgs
    {
        public SuccessOperationEventArgs( )
        {
           
        }
 
    }

    public class MessageEventArgs : EventArgs
    {
        public MessageEventArgs(string message, string date)
        {
            this.Message = message;
            this.Date = date;
        }
        public string Message { get; set; }
        public string Date { get; set; }
    }


    public class CalibrateImageSetEventArgs:EventArgs
    {
        public bool Success { get; set; }
        public CalibrateImageSetEventArgs(bool success)
        {
            this.Success = success;
        }
    }



    [Serializable]
    class ImageWorker
    {
        //    public WriteableBitmap image1 { get; private set; }
        //    public WriteableBitmap image2 { get; private set; }
        public List<WriteableBitmap> imageCal { get; private set; }
        public ObservableCollection<IndexedImage> images { get; private set; }
        public ObservableCollection<IndexedImage> origImages { get; private set; }
        public ObservableCollection<Log> Logs { get; private set; }
        //  private WriteableBitmap image1Orig;;
        //   private WriteableBitmap image2Orig;
        //public List<Uri> Paths { get; private set; }


    //    private List<Mat> imagesPoints;
    //    private List<Mat> objectPoints;
   //     private Mat[] RotVecs, TransVecs;
        private PointF[][] imagesPointsStereo;
        private Emgu.CV.Structure.MCvPoint3D32f[][] objectPointsStereo;

       

        private Matrix<double> RotVecsStereo, TransVecsStereo;

        private Matrix<double> OptimizedCameraMatrix;
       // Mat RotVecsStereo, TransVecsStereo;

        Matrix<double> Essential = new Matrix<double>(3, 3,1);
        Matrix<double> Fundamental = new Matrix<double>(3,3,1);

        Matrix<float> Camera,Distortion;

 
        Matrix<double> RectTransStereoFirst,RectTransStereoSecond,ProjStereoFisrt,ProjStereoSecond,DisparityToDepth;

        Matrix<double> CameraMatrix1 = new Matrix<double>(3, 3,1);// Mat(3, 3, DepthType.Cv64F, 1);
        Matrix<double>  Distortion1 = new Matrix<double>(1, 5);// Mat(8, 1, DepthType.Cv64F, 1);

        Matrix<double> CameraMatrix2 = new  Matrix<double>(3, 3,1);//Mat(3, 3, DepthType.Cv64F, 1);
        Matrix<double> Distortion2 = new Matrix<double>(1,5); //Mat(8, 1, DepthType.Cv64F, 1);


        private int ImgCounter;
        Uri imageCalibPath;

        public event EventHandler<ErrorEventArgs> errorEvent;
        public event EventHandler<SuccessOperationEventArgs> successEvent;
        public event EventHandler<CalibrateImageSetEventArgs> calibrateSetEvent;
        public event EventHandler<MessageEventArgs> msgEvent;

 
        public  bool Calibrated { get; private set; }
       
        public ImageWorker()
        {
            imageCal = new List<WriteableBitmap>();
            images = new ObservableCollection<IndexedImage>();
            origImages = new ObservableCollection<IndexedImage>();
            Logs = new ObservableCollection<Log>();
            this.msgEvent += ImageWorker_msgEvent;
            ImgCounter = 0;

  
        }

        private void ImageWorker_msgEvent(object sender, MessageEventArgs e)
        {
            Logs.Add(new Log(e.Message,e.Date));
        }

        [DllImport("gdi32")]
        private static extern int DeleteObject(IntPtr o);

        /// <summary>
        /// Convert an IImage to a WPF BitmapSource. The result can be used in the Set Property of Image.Source
        /// </summary>
        /// <param name="image">The Emgu CV Image</param>
        /// <returns>The equivalent BitmapSource</returns>
        public static BitmapSource ToBitmapSource(IImage image)
        {
            using (System.Drawing.Bitmap source = image.Bitmap)
            {
                IntPtr ptr = source.GetHbitmap(); //obtain the Hbitmap

                BitmapSource bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                    ptr,
                    IntPtr.Zero,
                    Int32Rect.Empty,
                    System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());

                DeleteObject(ptr); //release the HBitmap
                return bs;
            }
        }


       /* public BitmapSource Convert(System.Drawing.Bitmap bitmap)
        {
            var bitmapData = bitmap.LockBits(
                new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                System.Drawing.Imaging.ImageLockMode.ReadOnly, bitmap.PixelFormat);

            var bitmapSource = BitmapSource.Create(
                bitmapData.Width, bitmapData.Height, 96, 96, PixelFormats.Rgb24, null,
                bitmapData.Scan0, bitmapData.Stride * bitmapData.Height, bitmapData.Stride);

            bitmap.UnlockBits(bitmapData);
            return bitmapSource; 
        }*/

        public void ClearImages()
        {
            images.Clear();
            origImages.Clear();
            if (msgEvent != null)
            {
                msgEvent(this, new MessageEventArgs("Коллекция изображений очищена", DateTime.Now.ToString()));
            }
            ImgCounter = 0;

        }

        public void LoadImages(List<string> paths)
        {
           
                for (int i = 0; i < paths.Count; i++)
                {
                try
                {
                    images.Add(new IndexedImage(new WriteableBitmap(new BitmapImage(new Uri(paths[i]))), ImgCounter, paths[i]));
                    origImages.Add(new IndexedImage(new WriteableBitmap(new BitmapImage(new Uri(paths[i]))), ImgCounter, paths[i]));
                 //   Paths.Add(new Uri(paths[i]));
                    ImgCounter++;
                }
                catch (Exception ex)
                {
                    if (errorEvent != null)
                    {
                        errorEvent(this, new ErrorEventArgs(ex.Message, "Error loading image", MessageBoxImage.Error, MessageBoxButton.OK));
                    }
                }
                //   CvImag1 = Cv2.ImRead(path1);
                // CvImag2 = Cv2.ImRead(path2);
                
            }



                //   image1 =);
                //  image2 = new WriteableBitmap(new BitmapImage(new Uri(path2)));
                //   image1Orig = new WriteableBitmap(new BitmapImage(new Uri(path1)));
                //   image2Orig = new WriteableBitmap(new BitmapImage(new Uri(path2)));



            
           
            if (successEvent != null)
            {
                successEvent(this, new SuccessOperationEventArgs());
            }
            if (msgEvent != null)
            {
                msgEvent(this, new MessageEventArgs("Изображения загружены", DateTime.Now.ToString()));
            }
        }

        public void Reset()
        {
            for (int i = 0; i < images.Count; i++)
            {
                images[i].Image = origImages[i].Image;
            }
            if (msgEvent != null)
            {
                msgEvent(this, new MessageEventArgs("Изображения сброшены", DateTime.Now.ToString()));
            }
            
        }
        Mat[] Rotation = new Mat[1];
        Mat[] Transformation = new Mat[1];


        public void SetImageForCalibrate(string[] fNames, byte heightP, byte widthP)
        {
            bool success = false;
            
            //  imagesPoints.Clear();
            imageCal.Clear();
            int numP = heightP * widthP;
            MCvTermCriteria tm = new MCvTermCriteria(30, 0.001);
            //  Mat CamMatr = new Mat(3, 3, Emgu.CV.CvEnum.DepthType.Cv32F, 3);
            //Mat Dist = new Mat();

           
            Rotation[0] = new Mat();
            Transformation[0] = new Mat();

            Camera = new Matrix<float>(3, 3,1);
            Distortion= new Matrix<float>(8,1);
            imagesPoints = new PointF[fNames.Length][];
            objectPoints = new MCvPoint3D32f[fNames.Length][];
            int k = 0;
            foreach (var item in fNames)
            {
                try
                {

                    //Mat obj = new Mat(heightP,widthP,MatType.CV_USRTYPE1);
                    Emgu.CV.Structure.MCvPoint3D32f[] obj = new MCvPoint3D32f[numP];

                    for (int i = 0; i < numP; i++)
                    {
                        obj[i] = new MCvPoint3D32f(i / widthP, i % heightP, 0.0f);
                    }

                    var img = CvInvoke.Imread(item, Emgu.CV.CvEnum.LoadImageType.Grayscale);
                    //  Mat gray = new Mat();
                    // Cv2.CvtColor(img, gray, OpenCvSharp.ColorConversion.BgrToGray);
                    VectorOfPointF corn = new VectorOfPointF();



                    var chessSucc = CvInvoke.FindChessboardCorners(img, new System.Drawing.Size(widthP, heightP), corn);
                    
                    if (chessSucc)
                    {
                        CvInvoke.CornerSubPix(img, corn, new System.Drawing.Size(11, 11), new System.Drawing.Size(-1, -1), tm);
                        var imag = CvInvoke.Imread(item, Emgu.CV.CvEnum.LoadImageType.AnyColor);
                        CvInvoke.DrawChessboardCorners(imag, new System.Drawing.Size(11, 11), corn, chessSucc);
                        imageCal.Add(new WriteableBitmap(ToBitmapSource(imag)));
                        success = true;

                        imagesPoints[k] = corn.ToArray();
                        objectPoints[k] = obj;
                        k++;

                    }
                }

                catch (Exception ex)
                {
                    if (errorEvent != null)
                    {
                        errorEvent(this, new ErrorEventArgs(ex.Message, "Error loading image for calibrate", MessageBoxImage.Error, MessageBoxButton.OK));
                    }
                }


            }

            System.Drawing.Size imgSize = new System.Drawing.Size(Convert.ToInt16(imageCal[0].Width),Convert.ToInt16( imageCal[0].Height));
            var relCal = CvInvoke.CalibrateCamera(objectPoints, imagesPoints, imgSize, Camera, Distortion, CalibType.Default,tm, out Rotation, out Transformation);
            Calibrated = true;

       



            double m_error = 0;

            /* for (int i = 0; i < objectPoints.Count; i++)
             {
                 Mat imagePts = new Mat();
                 Cv2.ProjectPoints(objectPoints[i], RotVecs[i], TransVecs[i], CamMatr, Distortion, imagePts);
                 var error = Cv2.Norm(imagesPoints[i],imagePts, OpenCvSharp.NormType.L2)/imagePts.Height;
                 m_error += error;
             }///////////////

            if (calibrateSetEvent != null)
            {
                calibrateSetEvent(this, new CalibrateImageSetEventArgs(success));
            }*/
        }
        
        public void SetForCalibrate(string[] fNames)
        {
            foreach (var item in fNames)
            {
                imageCal.Add(new WriteableBitmap(new BitmapImage(new Uri(item))));
            }
        }
         

        public void SetImageForCalibrateStereo(string[] fNames, byte heightP, byte widthP)
        {
            bool success = false;

            //  imagesPoints.Clear();
            imageCal.Clear();
            int numP = heightP * widthP;

            int k = 0;
            int g = 0;
            int o = 0;
            MCvTermCriteria tm = new MCvTermCriteria(100, 0.001);
            imagesPointsStereo = new PointF[0][];
            objectPointsStereo = new MCvPoint3D32f[0][];
            PointF[][] imagesPts2 = new PointF[0][];
            RotVecsStereo =  new Matrix<double>(3,3,1);
            TransVecsStereo = new  Matrix<double>(3,1,1); //Mat(3,1, DepthType.Cv64F,1);
                                                          // Mat[] RotVecsStereo = new Mat[1];
                                                          // Mat[] TransVecsStereo = new Mat[1];
            if (fNames!=null)
            {
                foreach (var item in fNames)
                {
                    /* if (item.Contains("right"))
                     {
                         try
                         {

                             Emgu.CV.Structure.MCvPoint3D32f[] obj = new MCvPoint3D32f[numP];

                             for (int i = 0; i < numP; i++)
                             {
                                 obj[i] = new MCvPoint3D32f(i / widthP, i % heightP, 0.0f);
                             }

                             var img = CvInvoke.Imread(item, Emgu.CV.CvEnum.LoadImageType.Grayscale);

                             VectorOfPointF corn = new VectorOfPointF();

                             var chessSucc = CvInvoke.FindChessboardCorners(img, new System.Drawing.Size(heightP, widthP), corn);

                             if (chessSucc)
                             {
                                 Array.Resize<PointF[]>(ref imagesPts2, g + 1);
                                 Array.Resize<MCvPoint3D32f[]>(ref objectPointsStereo,o + 1);
                                 CvInvoke.CornerSubPix(img, corn, new System.Drawing.Size(11, 11), new System.Drawing.Size(-1, -1), tm);
                                 var image = CvInvoke.Imread(item, Emgu.CV.CvEnum.LoadImageType.Grayscale);
                                 CvInvoke.DrawChessboardCorners(image, new System.Drawing.Size(heightP, widthP), corn, chessSucc);
                                 var toCal = new WriteableBitmap(ToBitmapSource(image));
                                 imageCal.Add(toCal);
                                 success = true;
                                 imagesPts2[g] = corn.ToArray();
                                 objectPointsStereo[o] = obj;
                                 g++;
                                 o++;
                             }
                         }

                         catch (Exception ex)
                         {
                             if (errorEvent != null)
                             {
                                 errorEvent(this, new ErrorEventArgs(ex.Message, "Error loading image for calibrate", MessageBoxImage.Error, MessageBoxButton.OK));
                             }
                         }
                     }
                     else
                     {*/
                    try
                    {

                        Emgu.CV.Structure.MCvPoint3D32f[] obj = new MCvPoint3D32f[numP];

                        for (int i = 0; i < numP; i++)
                        {
                            obj[i] = new MCvPoint3D32f(i / widthP, i % heightP, 0.0f);
                        }

                        var img = CvInvoke.Imread(item, Emgu.CV.CvEnum.LoadImageType.Grayscale);

                        VectorOfPointF corn = new VectorOfPointF();

                        var chessSucc = CvInvoke.FindChessboardCorners(img, new System.Drawing.Size(heightP, widthP), corn);

                        if (chessSucc)
                        {
                            Array.Resize<PointF[]>(ref imagesPointsStereo, k + 1);
                            Array.Resize<MCvPoint3D32f[]>(ref objectPointsStereo, k + 1);
                            CvInvoke.CornerSubPix(img, corn, new System.Drawing.Size(11, 11), new System.Drawing.Size(-1, -1), tm);
                            var image = CvInvoke.Imread(item, Emgu.CV.CvEnum.LoadImageType.Grayscale);
                            CvInvoke.DrawChessboardCorners(image, new System.Drawing.Size(heightP, widthP), corn, chessSucc);
                            var toCal = new WriteableBitmap(ToBitmapSource(image));
                            imageCal.Add(toCal);
                            success = true;
                            imagesPointsStereo[k] = corn.ToArray();
                            objectPointsStereo[k] = obj;
                            k++;
                            o++;
                        }
                    }

                    catch (Exception ex)
                    {
                        if (errorEvent != null)
                        {
                            errorEvent(this, new ErrorEventArgs(ex.Message, "Error loading image for calibrate", MessageBoxImage.Error, MessageBoxButton.OK));
                        }
                    }




                }

                System.Drawing.Size imgSize = new System.Drawing.Size(System.Convert.ToInt32(imageCal[0].Width), System.Convert.ToInt32(imageCal[0].Height));
                //   Array.Resize<PointF[]>(ref imagesPointsStereo, 9);
                // Array.Resize<MCvPoint3D32f[]>(ref objectPointsStereo, 9);
                // Array.Resize<PointF[]>(ref imagesPts2, 9);
                Array.Resize<PointF[]>(ref imagesPts2, imagesPointsStereo.GetLength(0));
                imagesPointsStereo.CopyTo(imagesPts2, 0);

                try
                {
                    Mat[] r = new Mat[1];
                    Mat[] t = new Mat[1];

                    CvInvoke.StereoCalibrate(objectPointsStereo, imagesPointsStereo, imagesPts2, CameraMatrix1, Distortion1, CameraMatrix2, Distortion2, imgSize, RotVecsStereo, TransVecsStereo, Essential, Fundamental, CalibType.Default, tm);
                    //   CvInvoke.CalibrateCamera(objectPointsStereo, imagesPointsStereo, imgSize, CameraMatrix2, Distortion2, CalibType.Default, tm,out r, out t);
                    // CvInvoke.CalibrateCamera(objectPointsStereo, imagesPointsStereo, imgSize, CameraMatrix1, Distortion1, CalibType.Default, tm, out r, out t);



                    Calibrated = true;

                    if (calibrateSetEvent != null)
                    {
                        calibrateSetEvent(this, new CalibrateImageSetEventArgs(success));
                    }



                    double m_error = 0;
                }
                catch (Exception e)
                {
                    if (errorEvent != null)
                    {
                        errorEvent(this, new ErrorEventArgs("Неудалось калибровать камеру. Пoпробуйте ещё раз с другими изображениями.\n" + e.Message, "Ошибка", MessageBoxImage.Error, MessageBoxButton.OK));
                    }
                }
                /* for (int i = 0; i < objectPoints.Count; i++)
                 {
                     Mat imagePts = new Mat();
                     Cv2.ProjectPoints(objectPoints[i], RotVecs[i], TransVecs[i], CamMatr, Distortion, imagePts);
                     var error = Cv2.Norm(imagesPoints[i],imagePts, OpenCvSharp.NormType.L2)/imagePts.Height;
                     m_error += error;
                 }*/

                
            }
            else

            if (errorEvent != null)
            {
                errorEvent(this, new ErrorEventArgs("Не выбраны изображения для калибровки", "Ошибка", MessageBoxImage.Error, MessageBoxButton.OK));
            }


        }




        public void Zablurit() 
        {
           // var blur = CvImag1.Blur(new OpenCvSharp.CPlusPlus.Size(25, 30));
            
           // images[0] = new IndexedImage(new WriteableBitmap(Convert(OpenCvSharp.Extensions.BitmapConverter.ToBitmap(blur))),0);
            if (successEvent != null)
            {
                successEvent(this, new SuccessOperationEventArgs());
            }
            
        }

        private bool Undistorted = false;
        private PointF[][] imagesPoints;
        private MCvPoint3D32f[][] objectPoints;

        /* public void Rectify()
         {
             if (Calibrated)
             {
                 foreach (var item in origImages)
                 {

                     var size = new OpenCvSharp.CPlusPlus.Size(item.Image.Width, item.Image.Height);
                     var roi = new OpenCvSharp.CPlusPlus.Rect();
                     var OptimizedCamMatr = Cv2.GetOptimalNewCameraMatrix(CameraMatrix, Distortion, size, 1d, size, out roi);
                     ROI = roi;
                     OptimizedCameraMatrix = OptimizedCamMatr;
                     var img = Cv2.ImRead(item.Path);
                     Mat outImg = new Mat();
                     Cv2.Undistort(img, outImg, CameraMatrix, Distortion, OptimizedCamMatr);





                     string nameNew = item.Path.Substring(0, item.Path.Length - 4);
                     nameNew += "UNDISTORTED.png";
                     Cv2.ImWrite(nameNew, outImg);
                     for (int i = 0; i < origImages.Count; i++)
                     {
                         if (images[i].Index == item.Index)
                         {
                             images[i].Path = nameNew;
                             images[i].Image = new WriteableBitmap(Convert(OpenCvSharp.Extensions.BitmapConverter.ToBitmap(outImg)));
                             break;

                         }
                     }
                 }

                 Undistorted = true;
             }


         }*/

        public void RectifyWithStereo()
        {
            if (Calibrated)
            {
                 RectTransStereoFirst = new  Matrix<double>(3, 3,1);
                  RectTransStereoSecond = new   Matrix<double>(3, 3,1);
                ProjStereoFisrt = new  Matrix<double>(3, 4,1);
                 ProjStereoSecond = new  Matrix<double>(3, 4,1);
                 DisparityToDepth = new  Matrix<double>(4, 4,1);
                var size = new System.Drawing.Size(System.Convert.ToInt32(images[0].Image.Width), System.Convert.ToInt32(images[0].Image.Height));
                var sizeCal = new System.Drawing.Size(System.Convert.ToInt32(imageCal[0].Width), System.Convert.ToInt32(imageCal[0].Height));
                foreach (var item in images)
                {

                    var roi = new Rectangle(0,0, System.Convert.ToInt32(item.Image.Width), System.Convert.ToInt32(item.Image.Height));
                    item.ROI = roi; 
                }

                var roi1 = images[0].ROI;
                var roi2 = images[1].ROI;
                // var roi = new OpenCvSharp.CPlusPlus.Rect();
                try
                {
                    CvInvoke.StereoRectify(CameraMatrix1, Distortion1, CameraMatrix2, Distortion2, sizeCal, RotVecsStereo, TransVecsStereo, RectTransStereoFirst, RectTransStereoSecond, ProjStereoFisrt, ProjStereoSecond, DisparityToDepth, StereoRectifyType.Default,-1,size, ref roi1, ref roi2);
                    
                    
                    Mat NewCamMatr = new Mat();
                    Mat Map1 = new Mat();
                    Mat Map2 = new Mat();
                 /*   
                    CvInvoke.InitUndistortRectifyMap(CameraMatrix1, Distortion1, RectTransStereoFirst, NewCamMatr, size, DepthType.Cv32F, Map1, Map2);
              
                foreach (var item in origImages)
                {
 

                    Mat dst = new Mat();

                    CvInvoke.Remap(CvInvoke.Imread(item.Path, LoadImageType.Grayscale), dst, Map1, Map2, Inter.Linear);

                    string nameNew = item.Path.Substring(0, item.Path.Length - 4);
                    nameNew += "UNDISTORTED.png";
                    CvInvoke.Imwrite(nameNew, dst);
                    for (int i = 0; i < origImages.Count; i++)
                    {
                        if (images[i].Index == item.Index)
                        {
                            images[i].Path = nameNew;
                            images[i].Image = new WriteableBitmap(ToBitmapSource(dst));
                            break;

                        }
                    }
                }*/
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message+"\n"+e.StackTrace+"\n"+e.InnerException,"Err");

                }

                Undistorted = true;
            }
            else
            {
                if (errorEvent != null)
                {
                    errorEvent(this, new ErrorEventArgs("Камера не откалибрована.", "Недопустимое действие", MessageBoxImage.Warning, MessageBoxButton.OK));
                }
            }

           
        }

        /// <summary>
        /// Недоделанный метод сохранения в файл 3D модели. 
        /// </summary>
        /// <param name="space">Массив трёхмерных точек</param>
        public void WritePly(MCvPoint3D32f[] space)
        {
            BinaryWriter writer = new BinaryWriter(new FileStream("model.ply", FileMode.Create), Encoding.ASCII);
            //Write the headers for 3 vertices
            writer.Write(str2byteArray("ply\n"));
            writer.Write(str2byteArray("format binary_little_endian 1.0\n"));
            writer.Write(str2byteArray("element vertex 3\n"));
            writer.Write(str2byteArray("property float x\n"));
            writer.Write(str2byteArray("property float y\n"));
            writer.Write(str2byteArray("property float z\n"));
            writer.Write(str2byteArray("end_header\n"));
             
            string s="";
            //Write the 1st vertex
            // float[] In = new float[space.Width * space.Height];
            //Marshal.Copy(space.DataPointer, In, 0, space.Width * space.Height);
            /* for (int i = 0; i < In.GetLength(0)-3; i++)
             {
                 writer.Write(float2byteArray((float)In[i]));
                 writer.Write(float2byteArray((float)In[i+1]));
                 writer.Write(float2byteArray((float)In[i+2]));
             }*/
            for (int i = 0; i < space.GetLength(0); i++)
            {


                // writer.Write(float2byteArray((float)space[i].X));
                //writer.Write(float2byteArray((float)space[i].Y));
                //writer.Write(float2byteArray((float)space[i].Z));
                s += String.Format("v \t {0} \t {1} \t {2}",space[i].X,space[i].Y,space[i].Z);
                s += "\n"; 

            }
            s += "f 1//1 2//2 3//3 4//4";
            File.WriteAllText("mOdel.obj", s);
          

            //... write the remaining 2 vertices here

            //Close the binary writer
            writer.Close();
        }
        private byte[] float2byteArray(float value)
        {
            return BitConverter.GetBytes(value);
        }
        private byte[] str2byteArray(string theString)
        {
            return System.Text.Encoding.ASCII.GetBytes(theString);
        }


        /// <summary>
        /// Для реконструкции стереопары
        /// Обычный блок-матчер и репроекция точек
        /// </summary>
        public void CreateDepth()
        {
       //     if (Undistorted)
            {
                try
                {
                    var Imgl = CvInvoke.Imread(images[0].Path, LoadImageType.Grayscale);
                    var ImgR = CvInvoke.Imread(images[1].Path, LoadImageType.Grayscale);
                    int p1, p2;
                    p1 = 8 * 21 * Imgl.NumberOfChannels;
                    p2 = 8 * 21 * ImgR.NumberOfChannels;
                    Mat bareback = new Mat();
                    //CvInvoke.CalcOpticalFlowFarneback(Imgl, ImgR, output, 0.5, 3, 20, 10, 5, 1.2, OpticalflowFarnebackFlag.Default);
                    //  var stereo = new StereoBM(16, 15);
                    // var stereo = new StereoSGBM(0, 16, 11);

                    // stereo.Compute(Imgl, ImgR, output);
                    StereoSGBM stereoSolver = new StereoSGBM(0, 16, 15, p1, p2);
                    //using (StereoBM stereoSolver = new StereoBM(Emgu.CV.CvEnum.STEREO_BM_TYPE.BASIC, 0))
                    
                    stereoSolver.Compute(Imgl, ImgR, bareback);//Computes the disparity map using: 
                                                                                         /*GC: graph cut-based algorithm
                                                                                           BM: block matching algorithm
                                                                                           SGBM: modified H. Hirschmuller algorithm HH08*/
                    
                    


                    CvInvoke.Imwrite(images[0].Path + "STEREO.png", bareback);
                     

                    //CvInvoke.ReprojectImageTo3D(bareback, TriDe, DisparityToDepth);
                    var res = Emgu.CV.PointCollection.ReprojectImageTo3D(bareback, DisparityToDepth);
                    WritePly(res);
                    
                }
                catch (Exception e)
                {

                }
            }
        }


        public Matrix<float> ConcatDescriptors(IList<Matrix<float>> descriptors)
        {
            int cols = descriptors[0].Cols;
            int rows = descriptors.Sum(a => a.Rows);

            float[,] concatedDescs = new float[rows, cols];

            int offset = 0;

            foreach (var descriptor in descriptors)
            {
                // append new descriptors
                Buffer.BlockCopy(descriptor.ManagedArray, 0, concatedDescs, offset, sizeof(float) * descriptor.ManagedArray.Length);
                offset += sizeof(float) * descriptor.ManagedArray.Length;
            }

            return new Matrix<float>(concatedDescs);
        }

        /// <summary>
        /// Выбирает хорошие точки
        /// </summary>
        /// <param name="matches">Сопоставления</param>
        /// <param name="Descriptors1">Дескрипторы объекта</param>
        /// <param name="Descriptors2">Дескрипторы сцены</param>
        public VectorOfDMatch SelectGoodKeypoints(VectorOfVectorOfDMatch matches, Mat Descriptors1, Mat Descriptors2)
        {
            double min = 100; double max = 0;

            //-- Quick calculation of max and min distances between keypoints
            for (int i = 0; i < Descriptors1.Rows; i++)
            {
                double dist = matches[i][0].Distance;
                if (dist < min) min  = dist;
                if (dist > max ) max  = dist;
            }
            VectorOfDMatch good_matches = new VectorOfDMatch();
            MDMatch[] dm = new MDMatch[0];
            int k = 0;
            for (int i = 0; i < Descriptors1.Rows; i++)
            {
                
                if (matches[i][0].Distance < 4* min)
                {
                    Array.Resize<MDMatch>(ref dm, dm.GetLength(0) + 1);
                    dm[k] = matches[i][0];
                    k++;
                    
                }
            }
            good_matches.Push(dm);
            return good_matches;
        }

        /// <summary>
        /// Find correspondence to good matches keypoints on both images
        /// </summary>
        /// <param name="good">A good matches obtained from SelectGoodKeypoints</param>
        /// <param name="keypoints1">Keypoints from the first image</param>
        /// <param name="keypoints2">Keypoints from the second image</param>
        /// <returns></returns>
        public VectorOfPointF[] LocalizeKeypoints(VectorOfDMatch good, MKeyPoint[] keypoints1, MKeyPoint[] keypoints2)
        {
            //-- Localize the object
            PointF[][] objandscene = new PointF[2][];

            objandscene[0] = new PointF[good.Size];
            objandscene[1] = new PointF[good.Size];
            for (int i = 0; i < good.Size; i++)
            {
                
                //-- Get the keypoints from the good matches
                objandscene[0][i]=keypoints1[good[i].QueryIdx].Point;
                objandscene[1][i]=keypoints2[good[i].TrainIdx].Point;
            }
            VectorOfPointF[] vk = new VectorOfPointF[2];
            vk[0] = new VectorOfPointF(objandscene[0]);
            vk[1] = new VectorOfPointF(objandscene[1]);
             
            return vk;
        }
        /// <summary>
        /// Нахождение ключевых точек, соответсвий, отсеивание и вообще всё подряд
        /// </summary>
        public void FindKeypoints()
        {
            SIFT sift = new SIFT(2000);
            if (msgEvent != null)
            {
                msgEvent(this, new MessageEventArgs("Начало поиска ключевых точек и дескрипторов", DateTime.Now.ToString()));
            }

            // List<Matrix<float>> allDescriptors = new List<Matrix<float>>();
            foreach (var item in images)
            {
                //var img = CvInvoke.Imread(item.Path, LoadImageType.AnyColor);
                var gray = CvInvoke.Imread(item.Path, LoadImageType.Grayscale);

                var keyp = sift.Detect(gray);
                
                item.Keypoints =  new VectorOfKeyPoint(keyp);
               // Features2DToolbox.DrawKeypoints(img, item.Keypoints, gray, new Bgr(System.Drawing.Color.Red), Features2DToolbox.KeypointDrawType.Default);

                // item.KeypointImage = new WriteableBitmap(ToBitmapSource(gray));
                //item.Image = new WriteableBitmap(ToBitmapSource(gray));
                Mat descs = new Mat();
                
                sift.Compute(gray, item.Keypoints, descs);
                 
                item.Descriptors = descs;
              //  Matrix<float> mtr = new Matrix<float>(descs.Size);
                //descs.CopyTo(mtr);
                //allDescriptors.Add(mtr);
                
            }
            if (msgEvent != null)
            {
                msgEvent(this, new MessageEventArgs("Ключевые точки и дескрипторы найдены", DateTime.Now.ToString()));
            }


            if (msgEvent != null)
            {
                msgEvent(this, new MessageEventArgs("Начало поиска соотвествий", DateTime.Now.ToString()));
            }

            var search = new SearchParams(50);
 //      var index = new OpenCvSharp.CPlusPlus.Flann.IndexParams();
            var index_param = new KdTreeIndexParamses(5);

            BFMatcher Bmatcher = new BFMatcher(DistanceType.L2);
            VectorOfVectorOfDMatch matches = new VectorOfVectorOfDMatch();

            Bmatcher.Add(images[0].Descriptors);
            Bmatcher.KnnMatch(images[1].Descriptors, matches, 2, null);
           

            if (msgEvent != null)
            {
                msgEvent(this, new MessageEventArgs("Соответсвия найдены, количество:" +matches.Size.ToString(),DateTime.Now.ToString()));
            }

          /*=============================================== */
              
           // Mat fundamental = new Mat(new System.Drawing.Size(3,3), DepthType.Cv8U, 1);
            Mat homogr  = new Mat();

            List<VectorOfDMatch> good = new List<VectorOfDMatch>();
            for (int i = 0; i < matches.Size; i++)
            {
                if (matches[i][0].Distance < 0.75 * matches[i][1].Distance)
                    good.Add(matches[i]);
            }
           MDMatch[][] mdm = new MDMatch[good.Count][];

            for (int i = 0; i < good.Count; i++)
            {
                mdm[i] = good[i].ToArray();
            }

            VectorOfVectorOfDMatch blyat = new VectorOfVectorOfDMatch(mdm);
             
            var mask = new Mat(good.Count, 1, DepthType.Cv8U, 1);
            mask.SetTo(new MCvScalar(255));
            Features2DToolbox.VoteForUniqueness(blyat, 0.8, mask);
            int nonZeroCount = CvInvoke.CountNonZero(mask);


            if (nonZeroCount >= 4)
            {
                nonZeroCount = Features2DToolbox.VoteForSizeAndOrientation(images[0].Keypoints, images[1].Keypoints,blyat, mask, 1.5, 20);
                PointF[] kps1 = new PointF[images[0].Keypoints.Size];
                PointF[] kps2 = new PointF[images[1].Keypoints.Size];
                for (int i = 0; i < kps1.GetLength(0); i++)
                {
                    kps1[i] = images[0].Keypoints[i].Point;
                    kps2[i] = images[1].Keypoints[i].Point;
                }

                //        VectorOfPointF dpots1 = new VectorOfPointF(kps1);
                //      VectorOfPointF dpots2 = new VectorOfPointF(kps2);
                
                if (nonZeroCount >= 4)
                { 
                    try
                    {
                        homogr  = Features2DToolbox.GetHomographyMatrixFromMatchedFeatures(images[0].Keypoints, images[1].Keypoints, blyat, mask, 2);
                        // CvInvoke.FindFundamentalMat( , fundamental, FmType.Ransac);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message);
                    }
                }
            }
            if (msgEvent != null)
            {
                msgEvent(this, new MessageEventArgs("Выбраны хорошие соответсвия, количество: "+blyat.Size.ToString(), DateTime.Now.ToString()));
            } /*
=================       ==============================================================*/
         Mat res = new Mat();
         /*   int queryIdxs = 0, trainIdxs = 0;
            for (int i = 0; i < blyat.Size; i++)
            {
               var t=  blyat[i][0].QueryIdx;
                queryIdxs++;
            }
            for (int i = 0; i < blyat.Size; i++)
            {
                var t = blyat[i][1].TrainIdx;
                trainIdxs++;
            }*/
        
            //       images[0].Keypoints.FilterByKeypointSize(0, queryIdxs);
            //     images[1].Keypoints.FilterByKeypointSize(0, trainIdxs);
            if (msgEvent != null)
            {
                msgEvent(this, new MessageEventArgs("Начало отрисовки соответсвий и ключевых точек", DateTime.Now.ToString()));
            }

            var mats = SelectGoodKeypoints(matches,images[0].Descriptors,images[1].Descriptors);
            VectorOfVectorOfDMatch vvd = new VectorOfVectorOfDMatch(mats);
            Features2DToolbox.DrawMatches(CvInvoke.Imread(images[0].Path, LoadImageType.AnyColor), images[0].Keypoints,CvInvoke.Imread(images[1].Path, LoadImageType.AnyColor), images[1].Keypoints, vvd, res, new MCvScalar(255, 45, 15), new MCvScalar(36, 234, 199),null, Features2DToolbox.KeypointDrawType.NotDrawSinglePoints);
            if (msgEvent != null)
            {
                msgEvent(this, new MessageEventArgs("Отрисовано", DateTime.Now.ToString()));
            }
            //локализация точек до самых хороших
            var localized = LocalizeKeypoints(mats, images[0].Keypoints.ToArray(), images[0].Keypoints.ToArray());
            Mat fund = new Mat();
            Mat InliersMask = new Mat();

            ///Нахождение фундаменталньной матрицы
            ///
            //CvInvoke.FindFundamentalMat(localized[0], localized[1], fund, FmType.Ransac,3,0.99,InliersMask);
            string funds = "Funds:\n";
            
            ///Нужно матрицу поворота R и вектор перемещения t
            ///как?


           Mat homography = new Mat();
          
           CvInvoke.FindHomography(localized[0], localized[1], homography, HomographyMethod.Ransac );
            
        
            // Mat lines = new Mat();
            //CvInvoke.ComputeCorrespondEpilines(localized[0],0,fund,lines);

            ///Определение 
            ///расстояния до точек. 
            ///Результат -- 4-х мерная матрица
            ///
            Matrix<float> dim4 = new Matrix<float>( 4, localized[0].Size);
            CvInvoke.TriangulatePoints(ProjStereoFisrt, ProjStereoSecond, localized[0], localized[1], dim4);

            images[0].Image = new WriteableBitmap(ToBitmapSource(res));
            if (successEvent != null)
            {
                successEvent(this, new SuccessOperationEventArgs());
            }

            #region мусор

            /*          
           float[] data = new float[homography.Width * homography.Height];
           Marshal.Copy(homography.DataPointer, data, 0, homography.Width * homography.Height);
           for (int i = 0; i < data.GetLength(0); i++)
           {
               if (i % 3 == 0)
               {
                   funds += "\n";
               }
               funds += data[i].ToString() + " | ";
           }
           funds += "\n";
           funds += "homogr:\n";
           float[] In = new float[InliersMask.Width * InliersMask.Height];
           Marshal.Copy(InliersMask.DataPointer, In, 0, InliersMask.Width * InliersMask.Height);
           for (int i = 0; i < In.GetLength(0); i++)
           {
               if (i % 3 == 0)
               {
                   funds += "\n";
               }
               funds += In[i].ToString() + " | ";
           }

           VectorOfFloat inliers = new VectorOfFloat();
           Mat trans = new Mat();
           Mat rot = new Mat();
           VectorOfPoint3D32F pts = new VectorOfPoint3D32F(localized[0].Size);

           MessageBox.Show(funds);
          */
            try
            {
                // CvInvoke.SolvePnPRansac(localized[0], localized[1], Camera, Distortion, rot, trans, false, 400, 9, 60, inliers, SolvePnpMethod.P3P);
            }
            catch (Exception e)
            {

                // MessageBox.Show(e.Message+"\n");
            }



            /*float[] data = new float[fund.Width * fund.Height];
            Marshal.Copy(fund.DataPointer, data, 0, fund.Width * fund.Height);
            for (int i = 0; i < data.GetLength(0); i++)
            {
                if (i % 3== 0)
                {
                    funds += "\n";
                }
                funds += data[i].ToString() + " | ";
            }
            funds += "\n";
            funds += "Fundamental:\n";
            float[] d = new float[Fundamental.Width * Fundamental.Height];
            Marshal.Copy(Fundamental.DataPointer, d, 0, Fundamental.Width * Fundamental.Height);
            for (int i = 0; i < d.GetLength(0); i++)
            {
                if (i % 3 == 0)
                {
                    funds += "\n";
                }
                funds += d[i].ToString() + " | ";
            }

            MessageBox.Show(funds);
            */


            /*
                        MKeyPoint[] keyp = new MKeyPoint[blyat.Size];
                        MKeyPoint[] keyp1 = new MKeyPoint[blyat.Size];


                        for (int i = 0; i < blyat.Size; i++)
                        {
                           // blyat[0][0].
             //               foreach (var item in images[0].Keypoints[0].)
                            {
                                keyp[i] = blyat[i][0].QueryIdx;

                            }
                        }

                        VectorOfKeyPoint good1 = new VectorOfKeyPoint();


                        for (int i = 0; i < blyat.Size; i++)
                        {

                        }

                        */



            //  CvInvoke.FindFundamentalMat(images[0].Keypoints, images[1].Keypoints, homography);



            //Bmat.KnnMatch(allDescs,)
            //var allDescs = ConcatDescriptors(allDescriptors);
            //  float min_dist = 0;float max_dist = 0;
            //  index_param.SetAlgorithm(0);

            // var flann = new Emgu.CV.Flann.Index(allDescs, index_param);
            // var indic  = new Matrix<int>(allDescs.Rows, 2); // matrix that will contain indices of the 2-nearest neighbors found
            // var Sqdist = new Matrix<float>(allDescs.Rows, 2);
            // flann.KnnSearch(allDescs, indic, Sqdist,2, 24);

            /* for (int i = 0; i < indic.Rows; i++)
             {
                 var dist = Sqdist.Data[i, 0] -  Sqdist.Data[i, 1];
                 if (dist < min_dist)
                     min_dist = dist;
                 if (dist > max_dist)
                     max_dist = dist;
             }


             for (int i = 0; i < indic.Rows; i++)
             {
                 // filter out all inadequate pairs based on distance between pairs
                 if (Sqdist.Data[i, 0] < (0.6 * Sqdist.Data[i, 1]))
                 {




                     // find image from the db to which current descriptor range belongs and increment similarity value.
                     // in the actual implementation this should be done differently as it's not very efficient for large image collections.
                     foreach (var img in imap)
                     {
                         if (img.IndexStart <= i && img.IndexEnd >= i)
                         {
                             img.Similarity++;
                             break;
                         }
                     }
                 }
             }



             List<DMatch> Good = new List<DMatch>();
             List<Point2d> Points1 = new List<Point2d>();
             List<Point2d> Points2 = new List<Point2d>();

            // var matches = flann.KnnMatch(images[0].Descriptors, images[1].Descriptors, 2);
             foreach (var item in matches)
             {
                 var m = item[0];
                 var n = item[1];
                 if (item[0].Distance < 0.8 * item[1].Distance)
                 {
                     Good.Add(item[0]);
                     Points1.Add(new Point2d(images[0].Keypoints[item[0].QueryIdx].Pt.X, images[0].Keypoints[item[0].QueryIdx].Pt.Y));
                     Points2.Add(new Point2d(images[1].Keypoints[item[1].TrainIdx].Pt.X, images[1].Keypoints[item[1].TrainIdx].Pt.Y));
                 }
             }



             Mat Mask = new Mat();
             var FundamentMatrix = Cv2.FindFundamentalMat(Points1, Points2, OpenCvSharp.FundamentalMatMethod.LMedS,3,0.99,Mask);
            /* List<Point2d> InlierPoints1 = new List<Point2d>();
             List<Point2d> InlierPoints2 = new List<Point2d>();
             byte[,] MaskAr = new byte[Mask.Height,Mask.Width]; 

             Mask.GetArray(0,0,MaskAr);
             for (int i = 0; i < Points1.Count; i++)
             {
                 if (MaskAr[i,0] == 1)
                 {
                     InlierPoints1.Add(Points1[i]);
                     InlierPoints2.Add(Points2[i]);
                 }
             }*/
            #endregion

        }

    }
}
